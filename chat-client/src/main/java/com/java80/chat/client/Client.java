package com.java80.chat.client;

import com.java80.chat.client.ui.LoginMain;
import com.java80.chat.common.Consts;
import com.java80.chat.common.message.Header;
import com.java80.chat.common.message.LoginMessage;
import com.java80.chat.common.message.Message;
import com.java80.chat.common.user.User;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class Client {
	Bootstrap bs=null;
	static EventLoopGroup bossGroup=null;
	Channel ch=null;
	private LoginMain loginMain;
	public static void main(String[] args) {
		Client client = new Client();
		client.init();
		client.connectServer();
		
	}
	public void init(){
		bs=new Bootstrap();
		bossGroup=new NioEventLoopGroup();
		try {
			bs.group(bossGroup);
			bs.channel(NioSocketChannel.class); 
			bs.handler(new ClientChannelInitializer(loginMain));
			System.out.println("启动客户端 ... ...");
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	public void connectServer(){
		try {
			ChannelFuture channelFuture = bs.connect("127.0.0.1", 7973).sync();
			ch = channelFuture.channel();
			//ch.closeFuture().sync();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			//bossGroup.shutdownGracefully();
		}
		
	}
	public static void shutdown(){
		bossGroup.shutdownGracefully();
	}
	public void sendMessage(Message<?> msg){
		ch.writeAndFlush(msg);
	}
	public LoginMain getLoginMain() {
		return loginMain;
	}
	public void setLoginMain(LoginMain loginMain) {
		this.loginMain = loginMain;
	}
	
	
}
