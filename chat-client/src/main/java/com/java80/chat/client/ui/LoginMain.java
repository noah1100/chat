package com.java80.chat.client.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.java80.chat.client.Client;
import com.java80.chat.common.Consts;
import com.java80.chat.common.message.Header;
import com.java80.chat.common.message.LoginMessage;
import com.java80.chat.common.message.Message;
import com.java80.chat.common.user.User;

/** 
 * @author www.java80.com
 * @date 2017年6月12日 下午9:55:18 
 */
public class LoginMain extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int LOGIN_UI_WIDTH=360,LOGIN_UI_HEIGHT=230;
	public static final String LOGIN_UI_TITLE="Netty Client Login Frame";
	private Client client;
	private JTextField loginNameField;
	private JPasswordField loginPwdField;
	public void init(){
		JPanel contentPane=new JPanel();
		contentPane.setLayout(null);
		this.setContentPane(contentPane);
		
		loginNameField=new JTextField();
		loginPwdField=new JPasswordField();
		loginNameField.setBounds(50, 20, 200, 30);
		loginPwdField.setBounds(50,60,200,30);
		contentPane.add(loginPwdField);
		contentPane.add(loginNameField);
		JButton loginBtn=new JButton("Login");
		loginBtn.setBounds(130, 100, 80, 30);
		contentPane.add(loginBtn);
		
		loginBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				login();
			}
		});
		
		run();
	}
	public void login(){
		client.init();
		client.connectServer();
		Message<LoginMessage> ms=new Message<LoginMessage>();
		Header header=new Header();
		header.setCode(Consts.REQ_TYPE_LOGIN);
		header.setVersion((byte) 0);
		ms.setHeader(header);
		LoginMessage lm=new LoginMessage();
		lm.setUser(new User(0,loginNameField.getText(),new String(loginPwdField.getPassword())));
		ms.setContent(lm);
		client.sendMessage(ms);
	}
	public void initUI(){
		String lookAndFeel = UIManager.getSystemLookAndFeelClassName();
		try {
			UIManager.setLookAndFeel(lookAndFeel);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void openClientMain(){
		dispose();
		new ClientMain().start();
	}
	private void run(){
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(LOGIN_UI_WIDTH, LOGIN_UI_HEIGHT);
		this.setTitle(LOGIN_UI_TITLE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public void loginError() {
		JOptionPane.showMessageDialog(null, "用户密码错误", "提示",JOptionPane.ERROR_MESSAGE);
		client.shutdown();
	}
	
}
