package com.java80.chat.client;

import com.java80.chat.client.ui.LoginMain;
import com.java80.chat.common.Consts;
import com.java80.chat.common.message.Header;
import com.java80.chat.common.message.LoginResult;
import com.java80.chat.common.message.Message;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ClientHandler extends SimpleChannelInboundHandler<Message> {
	private LoginMain loginMain;
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Message msg) throws Exception {
		Header header = msg.getHeader();
		Object content = msg.getContent();
		short code = header.getCode();
		switch (code) {
		case Consts.RES_TYPE_LOGIN:
			LoginResult lres=(LoginResult) content;
			if(lres.isRes()){
				loginMain.openClientMain();
			}else{
				loginMain.loginError();
			}
			break;
		default:
			break;
		}
		
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		super.exceptionCaught(ctx, cause);
		Client.shutdown();
	}

	public ClientHandler(LoginMain loginMain) {
		super();
		this.loginMain = loginMain;
	}

	public ClientHandler() {
		super();
	}
	
}
