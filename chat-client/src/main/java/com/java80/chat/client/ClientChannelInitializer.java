package com.java80.chat.client;

import com.java80.chat.client.ui.LoginMain;
import com.java80.chat.common.codec.fastjson.RequestMessageDecoder;
import com.java80.chat.common.codec.fastjson.ResponseMessageEncoder;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class ClientChannelInitializer extends ChannelInitializer<SocketChannel>{
	
	@Override
	protected void initChannel(SocketChannel e) throws Exception {
		e.pipeline().addLast(new RequestMessageDecoder());
		e.pipeline().addLast(new ResponseMessageEncoder());
		e.pipeline().addLast(new ClientHandler(loginMain));
	}
	private LoginMain loginMain;
	public ClientChannelInitializer(LoginMain loginMain) {
		super();
		this.loginMain = loginMain;
	}
	public ClientChannelInitializer() {
		super();
	}
	
}
