package com.java80.chat.client.ui;

import javax.swing.JFrame;

import com.java80.chat.client.Client;

/** 
 * @author www.java80.com
 * @date 2017年6月12日 下午9:55:30 
 */
public class Start {
	public static void main(String[] args) {
		LoginMain loginFrame=new LoginMain();
		loginFrame.initUI();
		loginFrame.init();
		Client client=new Client();
		client.setLoginMain(loginFrame);
		loginFrame.setClient(client);
	}
}
