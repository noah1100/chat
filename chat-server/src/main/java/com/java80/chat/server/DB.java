package com.java80.chat.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.java80.chat.common.user.User;

import io.netty.channel.ChannelId;

/** 
 * 用户和通道对应
 * @author www.java80.com
 * @date 2017年6月11日 上午9:10:55 
 */
public class DB {
	private static Map<Integer,ChannelId> clients=new HashMap<Integer, ChannelId>();
	private static List<User> users=new ArrayList<User>();
	static{
		users=Arrays.asList(
				new User(1,"zxs","zxs"),
				new User(2,"xxx","xxx")
				);
	}
	/**
	 * @param userId
	 * @param cId
	 * @return
	 *  0 失败 1成功 2已有存在的 
	 */
	public static int addClient(int userId,ChannelId cId,boolean override){
		int res=1;
		try {
			if(clients.keySet().contains(userId)){
				if(override){
					clients.put(userId, cId);
					res=2;
				}else{
					res=2;
				}
			}else{
				clients.put(userId, cId);
				res=1;
			}
			
		} catch (Exception e) {
			res=0;
		}
		return res;
	}
	public static void removeClient(int userId){
		clients.remove(new Integer(userId));
	}
	public static void removeClient(ChannelId cId){
		Iterator<Entry<Integer, ChannelId>> iterator = clients.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, ChannelId> next = iterator.next();
			if(next.getValue().asLongText().equals(cId.asLongText())){
				iterator.remove();
			}
			
		}
	}
	public static boolean queryUser(String name,String pwd){
		for (User user : users) {
			if(user.getName().equals(name)&&user.getPwd().equals(pwd)){
				return true;
			}
		}
		return false;
	}
}
