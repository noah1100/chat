package com.java80.chat.server.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.java80.chat.server.Server;
import com.java80.chat.server.WorkerThreadMgr;

public class ServerMain extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int SERVER_UI_WIDTH=800, SERVER_UI_HEIGHT=700;
	public static String SERVER_UI_TITLE="Netty Server";
	private JPanel contentPane;
	private static JTextArea textArea;
	private static JLabel connectSize,onlineSize;
	private JButton startButton,restartButton,shutdownButton;
	Server serverService;
	public void init(){
		contentPane=new JPanel();
		contentPane.setLayout(null);
		this.setContentPane(contentPane);
		JScrollPane scrollPane = new JScrollPane();// 创建滚动面板
		scrollPane.setBounds(0, 0, 800, 600); 
		contentPane.add(scrollPane);// 应用面板
		textArea = new JTextArea();// 创建文本域
		textArea.setLineWrap(true); // 设置文本域自动换行
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);// 应用文本域
		
		JLabel connectSizeL=new JLabel("连接数：");
		JLabel onlineSizeL=new JLabel("在线人数：");
		connectSize=new JLabel("0");
		onlineSize=new JLabel("0");
		connectSizeL.setBounds(50, 600, 60, 30);
		connectSize.setBounds(110, 600, 60, 30);
		contentPane.add(connectSizeL);
		contentPane.add(connectSize);
		onlineSizeL.setBounds(170, 600, 60, 30);
		onlineSize.setBounds(230, 600, 60, 30);
		contentPane.add(onlineSizeL);
		contentPane.add(onlineSize);
		
		startButton=new JButton("启动");
		restartButton=new JButton("重启");
		shutdownButton=new JButton("停服");
		startButton.setBounds(300, 605, 60, 30);
		restartButton.setBounds(380, 605, 60, 30);
		shutdownButton.setBounds(460, 605, 60, 30);
		contentPane.add(startButton);
		contentPane.add(restartButton);
		contentPane.add(shutdownButton);
		shutdownButton.setEnabled(false);
		restartButton.setEnabled(false);
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WorkerThreadMgr.workerThreadService.execute(new Runnable() {
					public void run() {
						start();
						startButton.setEnabled(false);
						shutdownButton.setEnabled(true);
						restartButton.setEnabled(true);
					}
				});
				
			}
		});
		
		restartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WorkerThreadMgr.workerThreadService.execute(new Runnable() {
					public void run() {
						restart();
						startButton.setEnabled(false);
						shutdownButton.setEnabled(true);
						restartButton.setEnabled(true);
					}
				});
				
			}
		});
		
		shutdownButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WorkerThreadMgr.workerThreadService.execute(new Runnable() {
					public void run() {
						shutdown();
						startButton.setEnabled(true);
						shutdownButton.setEnabled(false);
						restartButton.setEnabled(false);
					}
				});
				
			}
		});
	}
	public static void main(String[] args) {
		ServerMain server = new ServerMain();
		server.initUI();
		server.init();
		server.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		server.setSize(SERVER_UI_WIDTH, SERVER_UI_HEIGHT);
		server.setTitle(SERVER_UI_TITLE);
		server.setLocationRelativeTo(null);
		server.setVisible(true);
		server.serverService = new Server();
	}
	public void initUI(){
		String lookAndFeel = UIManager.getSystemLookAndFeelClassName();
		try {
			UIManager.setLookAndFeel(lookAndFeel);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void writeLog(final String info){
		WorkerThreadMgr.workerThreadService.execute(new Runnable() {
			public void run() {
				textArea.append(info);
				textArea.append("\n");
			}
		});
	}
	public static void updateChannelSize(int size){
		connectSize.setText(size+"");
	}
	public static void updateOnlineSize(int size){
		onlineSize.setText(size+"");
	}
	public void start(){
		writeLog("服务器开始启动.....");
		WorkerThreadMgr.workerThreadService.execute(new Runnable() {
			public void run() {
				serverService.run();
			}
		});
		
	}
	public void restart(){
		writeLog("服务器开始重启.....");
		WorkerThreadMgr.workerThreadService.execute(new Runnable() {
			public void run() {
				serverService.shutdown();
				serverService.run();
			}
		});
	}
	public void shutdown(){
		serverService.shutdown();
		writeLog("服务器关闭.....");
	}
}
