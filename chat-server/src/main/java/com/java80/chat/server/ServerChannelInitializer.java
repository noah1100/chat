package com.java80.chat.server;

import com.java80.chat.common.codec.fastjson.RequestMessageDecoder;
import com.java80.chat.common.codec.fastjson.ResponseMessageEncoder;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/** 
 * @author www.java80.com
 * @date 2017年5月6日 下午8:33:55 
 */
public class ServerChannelInitializer extends ChannelInitializer<SocketChannel> {

	@Override
	protected void initChannel(SocketChannel e) throws Exception {
		// TODO Auto-generated method stub
		e.pipeline().addLast(new ResponseMessageEncoder());
		e.pipeline().addLast(new RequestMessageDecoder());
		e.pipeline().addLast(new ServerHandler());
	}
 
}
