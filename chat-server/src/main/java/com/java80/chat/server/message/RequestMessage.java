package com.java80.chat.server.message;
/** 
 * @author www.java80.com
 * @date 2017年5月6日 下午8:18:39 
 */
public class RequestMessage {
	private int type;
	private int len;
	private byte[] body;
	
	public int getLen() {
		return len;
	}
	public void setLen(int len) {
		this.len = len;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public byte[] getBody() {
		return body;
	}
	public void setBody(byte[] body) {
		this.body = body;
	}
}
