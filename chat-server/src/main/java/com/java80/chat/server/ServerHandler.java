package com.java80.chat.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.java80.chat.common.Consts;
import com.java80.chat.common.message.Header;
import com.java80.chat.common.message.LoginMessage;
import com.java80.chat.common.message.LoginResult;
import com.java80.chat.common.message.Message;
import com.java80.chat.common.user.User;
import com.java80.chat.server.ui.ServerMain;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

/** 
 * @author www.java80.com
 * @date 2017年5月6日 下午8:36:49 
 */
public class ServerHandler extends SimpleChannelInboundHandler<Message> {
	public static ChannelGroup group = new DefaultChannelGroup("ChannelGroups", GlobalEventExecutor.INSTANCE);
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Message msg) throws Exception {
		short code = msg.getHeader().getCode();
		ServerMain.writeLog("recv:->"+msg.toString());
		final Message tmp=msg;
		final ChannelId id = ctx.channel().id();
		switch (code) {
		case Consts.REQ_TYPE_LOGIN:
			WorkerThreadMgr.workerThreadService.execute(new Runnable() {
				public void run() {
					LoginMessage content = (LoginMessage) tmp.getContent();
					User user = content.getUser();
					Channel channel = group.find(id);
					Message<LoginResult> ms=new Message<LoginResult>();
					LoginResult rs=new LoginResult();
					boolean queryUser = DB.queryUser(user.getName(), user.getPwd());
					rs.setRes(queryUser);
					ms.setContent(rs);
					Header h=new Header();
					h.setCode(Consts.RES_TYPE_LOGIN);
					h.setVersion((byte) 0);
					ms.setHeader(h);
					ServerMain.writeLog("send:->"+ms.toString());
					if(queryUser){
						//DB.addClient(userId, cId, override);
					}
					channel.writeAndFlush(ms);
				}
			});
			break;

		default:
			break;
		}
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		group.add(ctx.channel());
		ServerMain.writeLog("客户端连接+1");
		ServerMain.updateChannelSize(group.size());
		super.channelActive(ctx);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		// TODO Auto-generated method stub
		group.remove(ctx.channel());
		ServerMain.writeLog("客户端连接-1 --> 异常");
		ServerMain.updateChannelSize(group.size());
		super.exceptionCaught(ctx, cause);
	}

	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		/*group.remove(ctx.channel());
		ServerMain.writeLog("客户端连接-1");
		ServerMain.updateChannelSize(group.size());*/
		super.handlerRemoved(ctx);
	} 
	
}
