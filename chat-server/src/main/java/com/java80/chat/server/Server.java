package com.java80.chat.server;


import com.java80.chat.server.ui.ServerMain;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/** 
 * @author www.java80.com
 * @date 2017年5月6日 下午8:32:42 
 */
public class Server {
	ServerBootstrap sb=null;
	EventLoopGroup bossGroup=null;
	EventLoopGroup workerGroup=null;
	
	public void run(){
		sb=new ServerBootstrap();
		bossGroup=new NioEventLoopGroup();
		workerGroup=new NioEventLoopGroup();
		try {
			sb.group(bossGroup, workerGroup);
			sb.channel(NioServerSocketChannel.class);
			sb.childHandler(new ServerChannelInitializer());
			ServerMain.writeLog("服务器启动成功...");
			Channel ch = sb.bind(7973).sync().channel();
			ch.closeFuture().sync();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}
	public void shutdown(){
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
		ServerMain.writeLog("服务器关闭成功...");
	}
	/*public static void main(String[] args) {
		new Server().run();
	}*/
}
