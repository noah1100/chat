package com.java80.chat.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class WorkerThreadMgr {
	public static final ExecutorService workerThreadService=newBlockingExecutorsUseCallerRun(4);
	public static ExecutorService newBlockingExecutorsUseCallerRun(int size) {
		return new ThreadPoolExecutor(size, size, 0L, TimeUnit.MILLISECONDS, new SynchronousQueue<Runnable>(),
				new RejectedExecutionHandler() {
					public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
						try {
							executor.getQueue().put(r);
						} catch (InterruptedException e) {
							throw new RuntimeException(e);
						}
					}
				});
	}
}
