package com.java80.chat.common.message;

public class LoginResult {
	private boolean res;

	public boolean isRes() {
		return res;
	}

	public void setRes(boolean res) {
		this.res = res;
	}

	@Override
	public String toString() {
		return "LoginResult [res=" + res + "]";
	}
	
}
