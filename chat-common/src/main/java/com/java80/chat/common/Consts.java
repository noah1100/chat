package com.java80.chat.common;
/** 
 * @author www.java80.com
 * @date 2017年5月7日 下午6:23:02 
 */
public class Consts {
	public static final short REQ_TYPE_LOGIN=1;
	public static final short REQ_TYPE_MESSAGE=2;
	public static final short REQ_TYPE_FILE=3;
	public static final short RES_TYPE_LOGIN=4;
}
