package com.java80.chat.common.codec.fastjson;

import java.util.List;

import com.java80.chat.common.Consts;
import com.java80.chat.common.message.Header;
import com.java80.chat.common.message.LoginMessage;
import com.java80.chat.common.message.LoginResult;
import com.java80.chat.common.message.Message;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/** 
 * @author www.java80.com
 * @date 2017年5月7日 下午6:17:40 
 */
public class RequestMessageDecoder extends ByteToMessageDecoder {
	final public static int HEAD_LENGTH = 7;
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		// TODO Auto-generated method stub
		if (in.readableBytes() < HEAD_LENGTH) { // 这个HEAD_LENGTH是我们用于表示头长度的字节数。
			return;
		}
		in.markReaderIndex(); // 我们标记一下当前的readIndex的位置
		//int dataLength = in.readInt(); // 读取传送过来的消息的长度。ByteBuf
		byte version=in.readByte();
		short code=in.readShort();
		int contentLen=in.readInt();
		Header header=new Header();
		header.setCode(code);
		header.setContentLen(contentLen);
		header.setVersion(version);
										// 的readInt()方法会让他的readIndex增加4
		if (contentLen < 0) { // 我们读到的消息体长度为0，这是不应该出现的情况，这里出现这情况，关闭连接。
			ctx.close();
		}
		if (in.readableBytes() < contentLen) { // 读到的消息体长度如果小于我们传送过来的消息长度，则resetReaderIndex.
			in.resetReaderIndex();
			return;
		}
		byte[] body = new byte[contentLen]; // 嗯，这时候，我们读到的长度，满足我们的要求了，把传送过来的数据，取出来吧~~
		in.readBytes(body); //
		Message msg=null;
		switch (code) {
		case Consts.REQ_TYPE_LOGIN:
			msg=new Message<LoginMessage>();
			LoginMessage loginMessage = FastjsonSerializeUtils.deserialize(body, LoginMessage.class);
			msg.setHeader(header);
			msg.setContent(loginMessage);
			break;
			
		case Consts.RES_TYPE_LOGIN:
			msg=new Message<LoginResult>();
			LoginResult loginResult = FastjsonSerializeUtils.deserialize(body, LoginResult.class);
			msg.setHeader(header);
			msg.setContent(loginResult);
			break;
			
		default:
			break;
		}
		if(code==Consts.REQ_TYPE_FILE){//文件消息
		}
		out.add(msg);
	}

}
