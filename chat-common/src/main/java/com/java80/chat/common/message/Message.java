package com.java80.chat.common.message;
/** 
 * @author www.java80.com
 * @date 2017年6月11日 下午4:18:44 
 */
public class Message<T>{
	private Header header;
	private T content;
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public T getContent() {
		return content;
	}
	public void setContent(T content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "Message [header=" + header + ", content=" + content + "]";
	}
	
}
