package com.java80.chat.common.message;
/** 
 * @author www.java80.com
 * @date 2017年6月11日 上午10:25:58 
 */
public class Header {
	private byte version;//版本 默认0
	private short code;//消息码
	private int contentLen;//内容长度
	public byte getVersion() {
		return version;
	}
	public void setVersion(byte version) {
		this.version = version;
	}
	public int getContentLen() {
		return contentLen;
	}
	public void setContentLen(int contentLen) {
		this.contentLen = contentLen;
	}
	public short getCode() {
		return code;
	}
	public void setCode(short code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return "Header [version=" + version + ", code=" + code + ", contentLen=" + contentLen + "]";
	}
	
}
