package com.java80.chat.common.codec.fastjson;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import com.alibaba.fastjson.serializer.SerializerFeature;

import io.netty.util.CharsetUtil;

/** 
 * @author www.java80.com
 * @date 2017年5月6日 下午6:22:26 
 */
public class FastjsonSerializeUtils {
	public static byte[] serialize(Object data){
		SerializeWriter writer=new SerializeWriter();
		JSONSerializer serialize = new JSONSerializer(writer);
		serialize.config(SerializerFeature.WriteEnumUsingToString, true);
		serialize.config(SerializerFeature.WriteClassName, true);
		serialize.write(data);
		return writer.toBytes(CharsetUtil.UTF_8);
	}
	public static <T> T deserialize(byte[] body,Class<T> clazz){
		return JSON.parseObject(body, clazz);
	}
}

