package com.java80.chat.common.message;

import com.java80.chat.common.user.User;

/** 
 * @author www.java80.com
 * @date 2017年6月11日 下午4:20:01 
 */
public class LoginMessage {
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "LoginMessage [user=" + user + "]";
	}
	
}
