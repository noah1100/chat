package com.java80.chat.common.codec.fastjson;


import com.java80.chat.common.Consts;
import com.java80.chat.common.message.Header;
import com.java80.chat.common.message.Message;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/** 
 * @author www.java80.com
 * @date 2017年5月7日 下午6:37:48 
 */
public class ResponseMessageEncoder extends MessageToByteEncoder<Message>{

	@Override
	protected void encode(ChannelHandlerContext ctx, Message msg, ByteBuf byteBuf) throws Exception {
		Header header = msg.getHeader();
		byteBuf.writeByte(header.getVersion());
		byteBuf.writeShort(header.getCode());
		//byteBuf.writeInt(header.getContentLen());
		byte[] body = null;
		int type = header.getCode();
		switch (type) {
		case Consts.REQ_TYPE_FILE:
			System.out.println("文件消息");
			break;
		case Consts.REQ_TYPE_LOGIN:
			System.out.println("登录消息");
			body = FastjsonSerializeUtils.serialize(msg.getContent());
			break;
		case Consts.REQ_TYPE_MESSAGE:
			System.out.println("普通消息");
			body = FastjsonSerializeUtils.serialize(msg.getContent());
			break;
		default:
			body = FastjsonSerializeUtils.serialize(msg.getContent());
			break;
		}
		byteBuf.writeInt(body.length);
        byteBuf.writeBytes(body);
	}
	
}
